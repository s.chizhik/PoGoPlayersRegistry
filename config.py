from telegram import Emoji

__bot_name__ = 'pogo_dp_players_registry_bot'
__bot_token__ = '261712540:AAEGvqfjKl1e0N8CkhXxUeopLeDcawwuUiI'
__admins__ = ['@s_chizhik', '@Anacsagor', '@Vit2005', '@RuslanLys']

__telegram_user_link__ = "[%s](http://telegram.me/%s)"

__info_commands__ = {
    'apk': ['apk.md'],
    'credits': ['credits.md'],
    'eevee': ['eevee.md'],
    'faq': ['faq.md'],
    'hello': ['hello.md'],
    'start': ['start.md'],
    'support': ['support.md'],
    'help': ['help.md'],
    'attack': ['attack_1.jpg', 'attack.md'],
    'levels': ['levels.md'],
    'calc': ['calc.md'],
    'egg': ['egg.md', 'egg_1.jpg'],
    'iv': ['iv.md'],
    'table': ['table_1.md', 'table_1.jpg', 'table_2.jpg', 'table_3.jpg', 'table_2.md'],
    'admins': ['admins.md'],
}

__info_images__ = {
    'table_1.jpg': 'AgADAgADsqcxG5xqmQ96G6w0tqnNEyUbcQ0ABO9piOPxIRax8XkAAgI',
    'table_2.jpg': 'AgADAgADsacxG5xqmQ9FJa8XbVKVYPMZcQ0ABM2i2pcFItDRkngAAgI',
    'table_3.jpg': 'AgADAgADsKcxG5xqmQ8PXhiGwlKycG4zcQ0ABBZiQZs7KGoVsXkAAgI',
    'egg_1.jpg': 'AgADAgADtKcxG5xqmQ90LI0qq51SLWcXcQ0ABCfp0KONWEMsWXgAAgI',
    'attack_1.jpg': 'AgADAgADs6cxG5xqmQ-GI2Oav_Bma1EKcQ0ABK-VaiMYH40AAah3AAIC',
}

__access_denied_messages__ = [
    'А по рукам не хо?! ' + Emoji.SMILING_FACE_WITH_HORNS,
    'Фу, кому сказал!',
    'Слишком много себе позволяешь, %username%!',
    'Сюда низя!',
    'Это не то, что тебе нужно. Уходи!'
]
