#!/usr/bin/env python3
import sqlite3

database = sqlite3.connect('database.sqlite3')
database.row_factory = sqlite3.Row

c = database.cursor()

teams = {
    'yellow': {
        'naming': "жёлтых",
        'players': [
            'Vit2005', 'ChemistDP', 'AlasarUa', 'ZaKosil', 'VikManz', 'Andry_Cat', 'asiK', 'winny82', 'moonkir',
            'riddels', 'strokecode', 'ulyanovn', 'AVesnin', 'IgorMagonov', 'argnist_dnepr', 'mrspidermanpls',
        ],
        'admins': [
            'Vit2005'
        ],
    },

    'red': {
        'naming': "красных",
        'players': [
            'Dem9ih4ik', 'DriveManiac', 'vrdp1', 'Andreykremer', 'Slimpak', 'BuHHu5', 'DeN4lk', 'FromDnepr',
            'lisa_v_kystax', 'Anwalt', 'bunchV', 'Dimaking', 'jivcheg', 'Roottb', 'Imboko', 'Eorneeton', 'and1lays',
            'ChibiRenny', 'Konst_b', 'BobaUA', 'x3ddd', 'Zxczggj', 'Yaroslav0202', 'Redfox969', 'Anasteziaua',
            'noshamee', 'linaanu', 'MaxTheTesla', 'Lakusya', 'M1R1K', 'RuslanLys'
        ],
        'admins': [
            'Anwalt', 'RuslanLys',
        ]
    },

    'blue': {
        'naming': "синих",
        'players': [
            'Amazzzonka', 'Anacsagor', 's_chizhik', 'Axul7', 'Spirt9go', 'GBoroda', 'Aoimoku', 'danatello10',
            'Masopav', 'PrayerMantis', 'TowaL17', 'Savalol', 'zAkDnepr', 'mitsuroseba', 'geka919', 'Imret',
            'VioletSmoke', 'morpheusua', 'TitusDP', 'eifanin', 'Slanapotam', 'p_ani', 'Kinori', 'Vorlock',
            'mortui', 'Nooblyk', 'Andryusha17', 'DiLesnoy', 'mysticdnp', 'Igorshmal', 'fer0x', 'kobe_8', 'JustBondik',
            'ysemeniuk', 'TrinaDp', 'Hegy_Hedgehog', 'Andrewwo12', 'roxyaria', 'VovaAlex', 'PhantomMaers', 'ALAYJUNE',
            'MadMasssta'
        ],
        'admins': [
            'Anacsagor', 's_chizhik',
        ],
    },

}

for team in teams:
    for player in teams[team]['players']:
        c.execute(
            "INSERT INTO players (telegram_login, team, created, changed) VALUES (?, ?, datetime('now'), datetime('now'));",
            (player, team))

    for admin in teams[team]['admins']:
        row = c.execute("SELECT id FROM players WHERE telegram_login = '%s'" % admin).fetchone()
        c.execute("INSERT INTO players_admin (player_id, created, changed) VALUES (%s, datetime('now'), datetime('now'))" % row['id'])

database.commit()

# We can also close the connection if we are done with it.
# Just be sure any changes have been committed or they will be lost.
database.close()
