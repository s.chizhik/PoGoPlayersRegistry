Администраторам фракционных чатов доступны команды:

 - /player\_create - добавление игрока
 - /player\_update - редактирование игрока
 - /player\_delete - удаление игрока

Форматы команд доступны при вызове команд без параметров.

Дополнительная информация доступна по командам:
 - /debug - `hostname` сервера
 - /dump - сделать backup базы данных
