Пользователи Реддита нашли способ получить нужную эволюцию Иви: достаточно переименовать его в одного из Eevee Brothers

```
Sparky => Jolteon
Rainer => Vaporeon
Pyro   => Flareon
```

*Эволюция работает один раз для каждого направления!*

Ссылки:
- [Pikabu](http://pikabu.ru/story/kak_poluchit_nuzhnuyu_yevolyutsiyu_ivi_4341022)
- [Reddit](https://www.reddit.com/r/pokemongo/comments/4t0cpo/psa_how_to_force_your_eevee_to_evolve_into_your/)
- [pokemongo](http://pokemongo.gamepress.gg/eevee-evolution)
