#!/usr/bin/env python3
import sqlite3

database = sqlite3.connect('database.sqlite3')

c = database.cursor()

c.execute("DROP TABLE IF EXISTS players")
c.execute("""
CREATE TABLE players
(
  id             INTEGER PRIMARY KEY AUTOINCREMENT,
  pogo_login     TEXT UNIQUE,
  telegram_login TEXT UNIQUE,
  team           TEXT,
  created        DATETIME,
  changed        DATETIME
)
""")

c.execute("DROP TABLE IF EXISTS players_admin")
c.execute("""
CREATE TABLE players_admin
(
  id             INTEGER PRIMARY KEY AUTOINCREMENT,
  player_id      INTEGER UNIQUE,
  created        DATETIME,
  changed        DATETIME,

  FOREIGN KEY(player_id) REFERENCES players(id)
)
""")

# Save (commit) the changes
database.commit()

# We can also close the connection if we are done with it.
# Just be sure any changes have been committed or they will be lost.
database.close()
