#!/usr/bin/env python3
import logging
import colorlog
import sys
import sqlite3
import random
import functools
import re

from telegram import Emoji
from telegram import ParseMode

from telegram.ext import Updater
from telegram.ext import CommandHandler
from telegram.ext import MessageHandler
import socket

import config

logger = colorlog.getLogger(__name__)

teams = {
    'yellow': {
        'full_naming': "Жёлтые, Инстинкт",
        'naming': "жёлтых",
        'icon': Emoji.YELLOW_HEART,
    },

    'red': {
        'full_naming': "Красные, Отвага",
        'naming': "красных",
        'icon': Emoji.HEAVY_BLACK_HEART,
    },

    'blue': {
        'full_naming': "Синие, Мистика",
        'naming': "синих",
        'icon': Emoji.BLUE_HEART,
    },
}

info_commands_cache = {}

database = sqlite3.connect('database.sqlite3', check_same_thread=False)
database.row_factory = sqlite3.Row

c = database.cursor()


def main():
    logger.info('Setting up application')

    updater = Updater(token=config.__bot_token__)
    dispatcher = updater.dispatcher

    for info_command in config.__info_commands__:
        dispatcher.add_handler(CommandHandler(info_command, info_callback(info_command)))

    dispatcher.add_handler(CommandHandler('yellow', team_callback('yellow')))
    dispatcher.add_handler(CommandHandler('red', team_callback('red')))
    dispatcher.add_handler(CommandHandler('blue', team_callback('blue')))

    dispatcher.add_handler(CommandHandler('teams', teams_all))

    dispatcher.add_handler(CommandHandler('debug', debug))
    dispatcher.add_handler(CommandHandler('ping', ping))
    dispatcher.add_handler(CommandHandler('dump', dump))

    dispatcher.add_handler(CommandHandler('player_create', player_create))
    dispatcher.add_handler(CommandHandler('player_update', player_update))
    dispatcher.add_handler(CommandHandler('player_delete', player_delete))

    dispatcher.add_handler(CommandHandler('whois', get_player_by('whois', 'telegram')))
    dispatcher.add_handler(CommandHandler('player', get_player_by('player', 'pogo')))
    dispatcher.add_handler(CommandHandler('image', image_test))

    dispatcher.add_handler(MessageHandler(callback=handle_message, filters=[]))

    logger.info('Start pooling')
    updater.start_polling()


def image_test(bot, update):
    log_input_message(update)

    for image_name in config.__info_images__:
        response = bot.sendPhoto(chat_id=update.message.chat_id,
                                 photo=open('./info/%s' % (config.__info_images__[image_name],), 'rb'))

        photos = response.photo
        last_photo = photos[len(photos) - 1]

        print("'%s': '%s'" % (image_name, last_photo.file_id))


def get_player_by(command_name, field):
    fetch_user_callback = db_get_player_by_telegram_login if field == 'telegram' else db_get_player_by_pogo_login
    login_desc = "логин в телеграме" if field == 'telegram' else "ник в Pokemon GO"

    def get_player_by_callback(bot, update):
        log_input_message(update)

        args = get_bot_args(update)

        if len(args) < 2:
            bot.sendMessage(chat_id=update.message.chat_id,
                            text="Формат команды:\n/%s <%s>" % (command_name, login_desc),
                            parse_mode=ParseMode.MARKDOWN)
            return

        login = normalize_telegram_login(args[1])

        user = fetch_user_callback(login)

        if user is not None:
            user_message = format_user_message(user)
        else:
            user_message = "Игрок `%s` не найден" % (login,)

        bot.sendMessage(chat_id=update.message.chat_id, text=user_message, parse_mode=ParseMode.MARKDOWN,
                        disable_web_page_preview=True)

    return get_player_by_callback


def player_create(bot, update):
    log_input_message(update)

    help_message = """
```
Формат команды:
/player_create <team> <telegram> <pokemon_go>

team       - команда yellow/red/blue
telegram   - username игрока в телеграме
pokemon_go - логин игрока в Pokemon GO
-----------------------------------------------
Пример:
/player_create blue @VasyaPupkinTelegram VasyaPupkin
```
"""
    username = update.message.from_user.name

    args = get_bot_args(update)

    if username not in config.__admins__:
        bot.sendMessage(chat_id=update.message.chat_id, text=access_denied_message())
        return

    if len(args) < 4:
        bot.sendMessage(chat_id=update.message.chat_id, text=help_message, parse_mode=ParseMode.MARKDOWN)
    else:
        team = args[1]
        telegram_login = normalize_telegram_login(args[2])
        pogo_login = args[3]

        if team not in teams:
            bot.sendMessage(chat_id=update.message.chat_id, text="Неверно задана команда `%s`" % (team,))
            return

        if db_get_player_by_pogo_login(pogo_login):
            bot.sendMessage(chat_id=update.message.chat_id, text="Игрок %s уже есть" % (pogo_login,))
            return

        if db_get_player_by_telegram_login(telegram_login):
            bot.sendMessage(chat_id=update.message.chat_id, text="Игрок %s уже есть" % (telegram_login,))
            return

        if db_add_player(team, telegram_login, pogo_login):
            user = db_get_player_by_telegram_login(telegram_login)
            user_message = format_user_message(user)
            bot.sendMessage(chat_id=update.message.chat_id, text="Игрок сохранён!")
            bot.sendMessage(chat_id=update.message.chat_id, text=user_message, parse_mode=ParseMode.MARKDOWN,
                            disable_web_page_preview=True)
        else:
            bot.sendMessage(chat_id=update.message.chat_id, text="Не удалось сохранить игрока. Ошибка залоггирована.")


def player_update(bot, update):
    log_input_message(update)

    help_message = """
```
Формат команды
/player_update <telegram> <new_telegram> <new_pokemon_go> <new_team>

telegram       - username игрока в телеграме
new_telegram   - новый username telegram
new_pokemon_go - логин игрока в Pokemon GO
new_team       - фракция игрока
-----------------------------------------------
Пример:
/player_update @VasyaPupkinTelegram @VasyaPupkinNewTelegram VasyaPupking blue
```
*По аргументу *`<telegram>`* ведётся поиск игрока.*
*Все аргументы обязательны!*
    """
    username = update.message.from_user.name

    args = get_bot_args(update)

    if username not in config.__admins__:
        bot.sendMessage(chat_id=update.message.chat_id, text=access_denied_message())
        return

    if len(args) < 5:
        bot.sendMessage(chat_id=update.message.chat_id, text=help_message, parse_mode=ParseMode.MARKDOWN)
    else:
        telegram_login = normalize_telegram_login(args[1])
        new_telegram_login = normalize_telegram_login(args[2])
        new_pogo_login = args[3]
        team = args[4]

        if team not in teams:
            bot.sendMessage(chat_id=update.message.chat_id, text="Неверно задана команда `%s`" % (telegram_login,))
            return

        if not db_get_player_by_telegram_login(telegram_login):
            bot.sendMessage(chat_id=update.message.chat_id,
                            text="Не найден игрок %s для редактирования" % (telegram_login,))
            return

        if db_update_player(telegram_login, new_telegram_login, new_pogo_login, team):
            user = db_get_player_by_telegram_login(new_telegram_login)
            user_message = format_user_message(user)
            bot.sendMessage(chat_id=update.message.chat_id, text="Игрок сохранён!")
            bot.sendMessage(chat_id=update.message.chat_id, text=user_message, parse_mode=ParseMode.MARKDOWN,
                            disable_web_page_preview=True)
        else:
            bot.sendMessage(chat_id=update.message.chat_id, text="Не удалось сохранить игрока. Ошибка залоггирована.")

    return


def player_delete(bot, update):
    log_input_message(update)

    username = update.message.from_user.name

    if username not in config.__admins__:
        bot.sendMessage(chat_id=update.message.chat_id, text=access_denied_message())
        return

    help_message = """
```
Формат команды
/player_delete <telegram>

telegram - username игрока в телеграме
-----------------------------------------------
Пример:
/player_delete @VasyaPupkinTelegram
```
"""

    args = get_bot_args(update)

    if len(args) != 2:
        bot.sendMessage(chat_id=update.message.chat_id, text=help_message, parse_mode=ParseMode.MARKDOWN)
    else:
        telegram_login = normalize_telegram_login(args[1])

        if not db_get_player_by_telegram_login(telegram_login):
            bot.sendMessage(chat_id=update.message.chat_id, text="Игрок @%s не найден." % telegram_login)
        else:
            if db_delete_player(telegram_login):
                bot.sendMessage(chat_id=update.message.chat_id, text="Игрок @%s был удалён." % telegram_login)
            else:
                bot.sendMessage(chat_id=update.message.chat_id, text="Не удалось удалить игрока.")


def info_callback(name) -> callable:
    logger.debug('Initializing /' + name)

    return functools.partial(invoke_info_send_message, name)


def invoke_info_send_message(name, bot, update):
    log_input_message(update)

    if name in config.__info_commands__:
        for file_name in config.__info_commands__[name]:

            if file_name.endswith('.md'):
                bot.sendMessage(chat_id=update.message.chat_id, text=info_commands_cache[file_name],
                                disable_web_page_preview=True, parse_mode=ParseMode.MARKDOWN)
            else:
                bot.sendPhoto(chat_id=update.message.chat_id,
                              photo=config.__info_images__[file_name])


def team_callback(name) -> callable:
    def team_callback_send_message(bot, update):
        log_input_message(update)

        bot.sendMessage(chat_id=update.message.chat_id,
                        text=format_team_message(name),
                        parse_mode=ParseMode.MARKDOWN,
                        disable_web_page_preview=True)

    return team_callback_send_message


def handle_message(bot, update):
    log_input_message(update)

    message = update.message.text

    if message.startswith('/'):
        bot.sendMessage(chat_id=update.message.chat_id, text="Команда \``%s`\` не реализована" % (message,),
                        parse_mode=ParseMode.MARKDOWN)

        # if update.message.new_chat_member:
        #     invoke_info_send_message('start', bot, update)
        #     print(update.message)
        # if update.message.left_chat_member:
        #     bot.sendMessage(chat_id=update.message.chat_id, text="Пока чувак")
        #     print(update.message)


def debug(bot, update):
    log_input_message(update)

    username = update.message.from_user.name
    debug_message = access_denied_message()

    if username in config.__admins__:
        debug_message = socket.gethostname()

    bot.sendMessage(chat_id=update.message.chat_id, text=debug_message)


def dump(bot, update):
    log_input_message(update)

    username = update.message.from_user.name

    if username in config.__admins__:
        bot.sendDocument(chat_id=update.message.chat_id, document=open('./database.sqlite3', 'rb'))
    else:
        bot.sendMessage(chat_id=update.message.chat_id, text=access_denied_message())


def teams_all(bot, update):
    log_input_message(update)

    teams_message = "\n------------\n".join(
        [format_team_message('yellow'), format_team_message('red'), format_team_message('blue')])

    bot.sendMessage(chat_id=update.message.chat_id,
                    text=teams_message,
                    parse_mode=ParseMode.MARKDOWN,
                    disable_web_page_preview=True)


# url: [text](URL)
def format_team_message(team):
    players = []
    admins = []

    for player in db_get_players(team):
        player_title = player['pogo_login'] if player['pogo_login'] else "@" + player['telegram_login']
        players.append(config.__telegram_user_link__ % (player_title, player['telegram_login']))

    for admin in db_get_players_admin(team):
        # TODO pogo_login
        admins.append(config.__telegram_user_link__ % (admin['telegram_login'], admin['telegram_login']))

    naming = teams[team]['naming']
    players_count = str(len(players))
    players = ", ".join(players)
    admins = ", ".join(admins)

    icon = teams[team]['icon']

    return "%s Команда *%s* (%s):\n%s\n\nПо вопросам добавления в командный чат обращайтесь к: %s" % (
        icon, naming, players_count, players, admins)


def format_user_message(user):
    user_link = config.__telegram_user_link__ % ("@" + user['telegram_login'], user['telegram_login'])

    if user['pogo_login'] is None:
        pogo_login = "<ник не задан>"
    else:
        pogo_login = user['pogo_login']

    team_naming = teams[user['team']]['full_naming']

    return """
*Игрок*: `%s`  %s
*Команда*: %s  %s
    """ % (pogo_login, user_link, team_naming, teams[user['team']]['icon'])


def ping(bot, update):
    logger.warn('Ping from %s' % (update.message.from_user.name,))
    bot.sendMessage(chat_id=update.message.chat_id, text="Реквестирую @s_chizhik...")


def prepare_logger():
    logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                        level=logging.INFO)

    formatter = colorlog.ColoredFormatter('%(log_color)s%(asctime)s [%(levelname)s] %(message)s', '%H:%M:%S')
    handler = logging.StreamHandler(sys.stdout)
    handler.setFormatter(formatter)

    logger.setLevel(logging.DEBUG)
    # logger.addHandler(handler)


def db_get_players(team):
    result = []

    c.execute("SELECT * FROM players WHERE team='%s' ORDER BY telegram_login;" % team)

    for row in c:
        result.append({
            'pogo_login': row['pogo_login'],
            'telegram_login': row['telegram_login'],
        })

    return result


def db_get_player_by_pogo_login(pogo_login):
    result = None

    try:
        c.execute("SELECT * FROM players WHERE pogo_login=?", (pogo_login,))

        for row in c:
            result = {
                'pogo_login': row['pogo_login'],
                'telegram_login': row['telegram_login'],
                'team': row['team'],
            }

    except BaseException:
        logger.error(sys.exc_info()[0])

    return result


def db_get_player_by_telegram_login(telegram_login):
    result = None

    try:
        c.execute("SELECT * FROM players WHERE telegram_login=?", (telegram_login,))

        for row in c:
            result = {
                'pogo_login': row['pogo_login'],
                'telegram_login': row['telegram_login'],
                'team': row['team'],
            }

    except BaseException:
        logger.error(sys.exc_info()[0])

    return result


def db_add_player(team, telegram_login, pogo_login):
    result = True

    try:
        c.execute("INSERT INTO players (team, telegram_login, pogo_login, created) VALUES (?, ?, ?,datetime('now'))",
                  (team, telegram_login, pogo_login))

        database.commit()
    except BaseException as e:
        result = False
        logger.exception(e)

    return result


def db_update_player(current_telegram_login, new_telegram_login, new_pogo_login, new_team):
    result = True

    try:
        c.execute(
            "UPDATE players SET team=?, telegram_login=?, pogo_login=?, changed=DATETIME('now') WHERE telegram_login=?",
            (new_team, new_telegram_login, new_pogo_login, current_telegram_login))

        database.commit()
    except BaseException as e:
        result = False
        logger.exception(e)

    return result


def db_delete_player(telegram_login):
    result = True

    try:
        c.execute(
            "DELETE FROM players WHERE telegram_login=?",
            (telegram_login,))

        database.commit()
    except BaseException as e:
        result = False
        logger.exception(e)

    return result


def db_get_players_admin(team):
    result = []

    c.execute("""
SELECT
  pogo_login,
  telegram_login
FROM players_admin, players
WHERE players_admin.player_id = players.id AND players.team = '%s'
""" % team)

    for row in c:
        result.append({
            'pogo_login': row['pogo_login'],
            'telegram_login': row['telegram_login'],
        })

    return result


def get_bot_args(update) -> list:
    message = update.message.text

    args = message.split(" ")

    return args


def get_info_contents(name):
    result = ""
    file = open('./info/%s' % (name,), 'r')

    while 1:
        line = file.readline()
        if not line:
            break
        result += line

    file.close()

    return result


def normalize_telegram_login(login: str) -> str:
    if login.startswith("@"):
        login = login[1:]

    return login


def log_input_message(update):
    logger.info("'%s' - via: %s" % (update.message.text, update.message.from_user.name))


def init_info_commands_cache():
    for command_name in config.__info_commands__:
        for file_name in config.__info_commands__[command_name]:
            if file_name.endswith('.md'):
                info_commands_cache[file_name] = get_info_contents(file_name)


def access_denied_message():
    return random.choice(config.__access_denied_messages__)


if __name__ == '__main__':
    init_info_commands_cache()
    prepare_logger()
    main()
